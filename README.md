# Time Computer

This is just a little Python script I wrote to figure the hours I work.
The date, job, and pairs of start time and end time should be entered in
a text file. This script takes that text file and figures hours worked
each day, week, job, etc.
